from unittest import TestCase


class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)


# zad. 9.2
def merge(node1, node2):
    """
    node1 and node2 are lists' heads
    """
    node = node1
    last = None
    while node:
        last = node
        node = node.next
    if last is None:
        return node2
    last.next = node2
    return node1


# zad 9.7
class TreeNode:
    def __init__(self, data=0, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


def count_leafs(top):
    if not top:
        return 0
    if not top.left and not top.right:
        return 1
    return count_leafs(top.left) + count_leafs(top.right)


def count_total(top):
    if not top:
        return 0
    return count_total(top.left) + top.data + count_total(top.right)


# helpers
def count_list_objects(head):
    node = head
    counter = 0
    while node:
        counter += 1
        node = node.next
    return counter


def print_list(head):
    node = head
    result = ''
    while node:
        data = '{0} '.format(node.data)
        result += data
        node = node.next
    print result


class TestBinaryTreeCount(TestCase):
    def setUp(self):
        self.right3 = TreeNode(data=2)
        self.right2 = TreeNode(data=3)
        self.left2 = TreeNode(data=3)
        self.right1 = TreeNode(data=4, right=self.right3)
        self.left1 = TreeNode(data=4, left=self.left2, right=self.right2)
        self.top = TreeNode(data=5, left=self.left1, right=self.right1)

    def test_count_leafs(self):
        result = count_leafs(self.top)
        result_single = count_leafs(self.right3)
        self.assertEqual(result, 3)
        self.assertEqual(result_single, 1)

    def test_count_leafs_no_top(self):
        result = count_leafs(None)
        self.assertEqual(result, 0)

    def test_count_total(self):
        result = count_total(self.top)
        result_single = count_total(self.left2)
        self.assertEqual(result, 21)
        self.assertEqual(result_single, 3)

    def test_count_total_no_top(self):
        result = count_total(None)
        self.assertEqual(result, 0)


class TestMergeNoSingleList(TestCase):
    def setUp(self):
        self.node13 = Node(data=13)
        self.node12 = Node(data=12, next=self.node13)
        self.node11 = Node(data=11, next=self.node12)
        self.head1 = Node(data=10, next=self.node11)

        self.node22 = Node(data=22)
        self.node21 = Node(data=21, next=self.node22)
        self.head2 = Node(data=20, next=self.node21)

    def test_count_list_object(self):
        self.assertEqual(count_list_objects(self.head1), 4)
        self.assertEqual(count_list_objects(self.head2), 3)

    def test_merge(self):
        head3 = merge(self.head1, self.head2)
        self.assertEqual(head3, self.head1)
        self.assertEqual(self.node13.next, self.head2)
        self.assertEqual(count_list_objects(head3), 7)

    def test_merge_second_list_empty(self):
        head3 = merge(self.head1, None)
        self.assertEqual(head3, self.head1)
        self.assertEqual(count_list_objects(head3), 4)

    def test_merge_first_list_empty(self):
        head3 = merge(None, self.head2)
        self.assertEqual(head3, self.head2)
        self.assertEqual(count_list_objects(head3), 3)


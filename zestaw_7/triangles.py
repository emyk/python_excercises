# coding=utf-8
# Emilia Marczyk, gr. 2
from sympy import Symbol, Eq, solve
from zestaw_6.points import Point


class Triangle:
    """Klasa reprezentująca trójkąty na płaszczyźnie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0, x3=0, y3=0):
        # Należy zabezpieczyć przed sytuacją, gdy punkty są współliniowe.
        self.check_collinear(x1, y1, x2, y2, x3, y3)
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)
        self.pt3 = Point(x3, y3)
        self.all_points = [self.pt1, self.pt2, self.pt3]

    def check_collinear(self, x1, y1, x2, y2, x3, y3):
        a = Symbol('a')
        b = Symbol('b')
        if x1 == x2 == x3 or y1 == y2 == y3:
            raise ValueError
        if solve([Eq(a*x1 + b, y1), Eq(a*x2 + b, y2), Eq(a*x3 + b, y3)], [a, b]):
            raise ValueError

    def __str__(self):         # "[(x1, y1), (x2, y2), (x3, y3)]"
        return "[{0}, {1}, {2}]".format((self.pt1.x, self.pt1.y), (self.pt2.x, self.pt2.y), (self.pt3.x, self.pt3.y))

    def __repr__(self):        # "Triangl.e(x1, y1, x2, y2, x3, y3)"
        return "Triangle({0}, {1}, {2}, {3}, {4}, {5})".format(self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y, self.pt3.x, self.pt3.y)

    def __eq__(self, other):   # obsługa tr1 == tr2
        other_points = [(point.x, point.y) for point in other.all_points]
        my_points = [(point.x, point.y) for point in self.all_points]
        return sorted(my_points) == sorted(other_points)

    def __ne__(self, other):        # obsługa tr1 != tr2
        return not self == other

    def center(self):          # zwraca środek trójkąta
        ox = (self.pt1.x + self.pt2.x + self.pt3.x)/3
        oy = (self.pt1.y + self.pt2.y + self.pt3.y)/3
        return ox, oy

    def area(self):            # pole powierzchni
        return abs(self.pt1.x * (self.pt2.y - self.pt3.y) + self.pt2.x * (self.pt3.y - self.pt1.y) + self.pt3.x * (self.pt1.y - self.pt2.y) / 2)

    def move(self, x, y):      # przesunięcie o (x, y)
        for point in self.all_points:
            point.x += x
            point.y += y
        return self

    def make4(self):           # zwraca listę czterech mniejszych
        pt1_pt2_mid = Point((self.pt1.x + self.pt2.x)/2, (self.pt1.y + self.pt2.y)/2)
        pt2_pt3_mid = Point((self.pt2.x + self.pt3.x)/2, (self.pt2.y + self.pt3.y)/2)
        pt3_pt1_mid = Point((self.pt3.x + self.pt1.x)/2, (self.pt3.y + self.pt1.y)/2)
        tr1 = Triangle(self.pt1.x, self.pt1.y, pt1_pt2_mid.x, pt1_pt2_mid.y, pt3_pt1_mid.x, pt3_pt1_mid.y)
        tr2 = Triangle(self.pt2.x, self.pt2.y, pt2_pt3_mid.x, pt2_pt3_mid.y, pt1_pt2_mid.x, pt1_pt2_mid.y)
        tr3 = Triangle(self.pt3.x, self.pt3.y, pt3_pt1_mid.x, pt3_pt1_mid.y, pt2_pt3_mid.x, pt2_pt3_mid.y)
        tr4 = Triangle(pt1_pt2_mid.x, pt1_pt2_mid.y, pt2_pt3_mid.x, pt2_pt3_mid.y, pt3_pt1_mid.x, pt3_pt1_mid.y)
        return [tr1, tr2, tr3, tr4]

# Kod testujący moduł.

import unittest


class TestTriangle(unittest.TestCase):
    def setUp(self):
        self.triangle = Triangle(0, 0, 0, 2, 2, 0)
        self.triangle_equal = Triangle(2, 0, 0, 0, 0, 2)
        self.triangle2 = Triangle(3, 0, 6, 3, 9, 0)

    def test_repr(self):
        self.assertEqual(self.triangle.__repr__(), "Triangle(0, 0, 0, 2, 2, 0)")

    def test_str(self):
        self.assertEqual(self.triangle.__str__(), "[(0, 0), (0, 2), (2, 0)]")

    def test_equal(self):
        self.assertEqual(self.triangle, self.triangle_equal)

    def test_not_equal(self):
        self.assertNotEqual(self.triangle, self.triangle2)

    def test_collinear(self):
        self.assertRaises(ValueError, Triangle, 1, 1, 2, 2, 3, 3)
        self.assertRaises(ValueError, Triangle, 1, 1, 1, 2, 1, 3)
        self.assertRaises(ValueError, Triangle, 1, 1, 2, 1, 2, 1)
        self.assertRaises(ValueError, Triangle, 1, 1, 2, 2, 1, 1)

    def test_center(self):
        self.assertEqual(self.triangle2.center(), (6, 1))

    def test_area(self):
        self.assertEqual(self.triangle.area(), 2)

    def test_move(self):
        self.assertEqual(self.triangle.move(2, 3), Triangle(2, 3, 2, 5, 4, 3))

    def test_make4(self):
        tr1 = Triangle(0, 0, 1, 0, 0, 1)
        tr2 = Triangle(1, 0, 2, 0, 1, 1)
        tr3 = Triangle(1, 1, 0, 2, 0, 1)
        tr4 = Triangle(0, 1, 1, 1, 1, 0)
        self.assertEqual(sorted(self.triangle.make4()), sorted([tr1, tr2, tr3, tr4]))

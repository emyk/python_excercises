# coding=utf-8
# Emilia Marczyk, gr. 2

line = "This \nis a word\twith\nwhite characters "
splitted_line = line.split()
print "Line: \"{0}\"\n".format(line)

# Zad. 2.10
amount = len(splitted_line)
print "Number of words in line: ", amount, "\n"

# Zad. 2.11
word = 'something'
modified = '_'.join(word)

print "Word: ", word
print "Word with underscores: ", modified, "\n"

# Zad. 2.12
first_letters = ''.join([i[0] for i in splitted_line])
last_letters = ''.join(i[-1] for i in splitted_line)

print "Word from first letters: ", first_letters
print "Word from last letters: ", last_letters, "\n"

# Zad. 2.13
line_length = sum([len(j) for j in splitted_line])
print "Line words length: ", line_length, "\n"

# Zad. 2.14
longest_word = max(splitted_line, key=len)
len_of_longest_word = len(longest_word)

print "a) Longest word: ", longest_word
print "b) Its length: ", len_of_longest_word, "\n"

# Zad. 2.15
L = [2, 6, 4, 2, 56, 87, 234, 1]
str_from_number_list = ''.join(str(i) for i in L)
print "List: ", L
print "Str from list: ", str_from_number_list, "\n"

# Zad. 2.16
line_gvr = "asdGvRos adoGvR GvR"
replaced = line_gvr.replace('GvR', 'Guldo van Rossum')
print "GvR str: ", line_gvr
print "Replaced: ", replaced, "\n"

# Zad. 2.17
sorted_by_words_length = sorted(splitted_line, key=len)
sorted_alphabetically = sorted(splitted_line, key=str.lower)
# sort/sorted domy�lnie preferuje s�owa zaczynaj�ce si� od wielkiej litery,
# dlatego trzeba zaznaczy�, �eby sortowa� niezale�nie od wielko�ci liter - st�d str.lower
print "Sorted by words length: ", sorted_by_words_length
print "Sorted alphabetically: ", sorted_alphabetically, "\n"

# Zad. 2.18
big_number_with_zeros = 18276380293610002830
zeros = str(big_number_with_zeros).count('0')
print "Number: ", big_number_with_zeros
print "Zeros in it: ", zeros, "\n"

# Zad. 2.19
L = [1, 45, 678, 2, 43, 980]
L2 = [str(l).zfill(3) for l in L]
print "List to fill", L
print "Filled list: ", L2
import random
from unittest import TestCase


class RandomQueueException(Exception):
    pass


class RandomQueue:
    def __init__(self):
        self.elements = []
        self.n = 0

    def insert(self, item):
        self.elements.append(item)
        self.n += 1

    def remove(self):  # zwraca losowy element
        if self.is_empty():
            raise RandomQueueException("Queue is empty!")
        index = random.randint(0, self.n-1)
        self.n -= 1
        to_remove = self.elements[index]
        last_item = self.elements.pop()
        if to_remove == last_item:
            return last_item
        self.elements[index] = last_item
        return to_remove

    def is_empty(self):
        return self.n == 0


class TestRandomQueue(TestCase):
    def setUp(self):
        self.queue = RandomQueue()

    def test_insert(self):
        self.queue.insert('a')
        self.assertEqual(self.queue.n, 1)
        self.assertEqual(self.queue.elements, ['a'])

    def test_remove(self):
        for i in xrange(10):
            self.queue.insert(i*10)
        self.assertEqual(self.queue.n, 10)
        element = self.queue.remove()
        self.assertEqual(self.queue.n, 9)
        self.assertNotIn(element, self.queue.elements)

    def test_remove_from_empty_queue(self):
        self.assertEqual(self.queue.n, 0)
        self.assertRaises(RandomQueueException, self.queue.remove)

    def test_remove_queue_single_element(self):
        self.queue.insert(1000)
        removed = self.queue.remove()
        self.assertEqual(removed, 1000)
        self.assertEqual(self.queue.n, 0)

    def test_is_empty(self):
        self.assertTrue(self.queue.is_empty(), "Queue is not empty! Contains: %s " % self.queue.elements)

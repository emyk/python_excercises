# -*- coding: utf-8 -*-
from unittest import TestCase


class PriorityQueueException(Exception):
    pass


# Zad. 10.6
class PriorityQueue:
    def __init__(self, size=10):
        self.items = size * [None]
        self.n = 0      # pierwszy wolny indeks
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def insert(self, data):
        if self.is_full():
            raise PriorityQueueException('Queue is full!')
        self.items[self.n] = data
        self.n += 1

    def remove(self):
        if self.is_empty():
            raise PriorityQueueException('Queue is empty!')
        # Etap 1 - wyszukiwanie elementu.
        maxi = 0
        for i in range(self.n):
            if self.items[i] > self.items[maxi]:
                maxi = i
        # Etap 2 - usuwanie elementu.
        self.n -= 1
        data = self.items[maxi]
        self.items[maxi] = self.items[self.n]
        self.items[self.n] = None  # usuwamy referencję
        return data

    def increase(self, value):
        for i in xrange(self.n):
            if self.items[i] == value:
                self.items[i] += 1


class TestPriorityQueue(TestCase):
    def setUp(self):
        self.queue = PriorityQueue(size=4)

    def test_insert(self):
        self.queue.insert(9)
        self.assertEqual(self.queue.n, 1)
        self.assertIn(9, self.queue.items)

    def test_insert_to_full_queue(self):
        for i in xrange(4):
            self.queue.insert(i)
        self.assertEqual(self.queue.n, 4)
        self.assertRaises(PriorityQueueException, self.queue.insert, 9)

    def test_remove(self):
        self.queue.insert(5)
        self.queue.insert(8)
        self.queue.insert(2)
        removed = self.queue.remove()
        self.assertEqual(removed, 8)

    def test_remove_from_empty_queue(self):
        self.assertEqual(self.queue.n, 0)
        self.assertRaises(PriorityQueueException, self.queue.remove)

    def test_increase(self):
        self.queue.insert(1)
        self.queue.insert(2)
        self.queue.insert(3)
        self.assertIn(2, self.queue.items)
        self.queue.increase(2)
        self.assertNotIn(2, self.queue.items)
        self.assertEqual(self.queue.items.count(3), 2)


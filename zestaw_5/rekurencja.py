# Emilia Marczyk


def factorial(n):
    result = 1
    for i in xrange(n+1):
        if i == 0:
            result *= 1
        else:
            result *= i
    return result


def fibonacci(n):
    i_pp = 0
    i_p = 1
    if n == 0:
        return 0
    if n == 1:
        return 1

    for i in xrange(2, n+1):
        suma = i_pp + i_p
        i_pp = i_p
        i_p = suma
    return i_p

# -*- coding: utf-8 -*-
from unittest import TestCase


class StackException(Exception):
    pass


# Zad. 10.2
class Stack:
    def __init__(self, size=10):
        self.items = size * [None]      # utworzenie tablicy
        self.n = 0                      # liczba elementów na stosie
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def push(self, data):
        if self.is_full():
            raise StackException('Stack is full!')
        self.items[self.n] = data
        self.n += 1

    def pop(self):
        if self.is_empty():
            raise StackException("Stack is empty!")
        self.n -= 1
        data = self.items[self.n]
        self.items[self.n] = None    # usuwam referencję
        return data


class TestStack(TestCase):
    def setUp(self):
        self.stack = Stack(size=3)

    def test_push_ok(self):
        self.assertEqual(self.stack.n, 0)
        self.stack.push(1)
        self.assertEqual(self.stack.n, 1)

    def test_pop_ok(self):
        self.stack.push(1)
        self.assertEqual(self.stack.n, 1)
        removed_item = self.stack.pop()
        self.assertEqual(self.stack.n, 0)
        self.assertEqual(removed_item, 1)

    def test_pop_empty_stack(self):
        self.assertEqual(self.stack.n, 0)
        self.assertRaises(StackException, self.stack.pop)

    def test_push_to_full_stack(self):
        for i in xrange(3):
            self.stack.push(i)
        self.assertEqual(self.stack.n, self.stack.size)
        self.assertRaises(StackException, self.stack.push, 4)

    def test_is_empty(self):
        self.assertTrue(self.stack.is_empty(), "Stack is not empty! Contains: %s " % self.stack.items)

    def test_is_full(self):
        for i in xrange(3):
            self.stack.push(i)
        self.assertTrue(self.stack.is_full(), "Stack is not full. size: %s, elements: %s" % (self.stack.size, self.stack.n))

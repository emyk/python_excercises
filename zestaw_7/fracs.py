# coding=utf-8
# Emilia Marczyk, gr. 2
from zestaw_5.fracs import add_frac, sub_frac, get_new_denominator_and_numerators


class Frac:
    """Klasa reprezentująca ułamki."""

    def __init__(self, x=0, y=1):
        # Sprawdzamy, czy y=0.
        self.x = self.get_x(x)
        self.y = self.get_y(y)

    def get_x(self, x):
        if not isinstance(x, (int, float, long)):
            raise ValueError
        return x

    def get_y(self, y):
        if y == 0 or not isinstance(y, (int, float, long)):
            raise ValueError
        return y

    def __str__(self):         # zwraca "x/y" lub "x" dla y=1
        return "{0}".format(self.x) if self.y == 1 else "{0}/{1}".format(self.x, self.y)

    def __repr__(self):        # zwraca "Frac(x, y)"
        return "Frac({0}, {1})".format(self.x, self.y)

    def __cmp__(self, other):  # porównywanie
        y, x1, x2 = get_new_denominator_and_numerators([self.x, self.y], [other.x, other.y])
        if x1 == x2:
            return 0
        return -1 if x1 < x2 else 1

    def __add__(self, other):  # frac1+frac2, frac+int
        if isinstance(other, float):
            ratio = float.as_integer_ratio(other)
            return self + Frac(ratio[0], ratio[1])
        if isinstance(other, int):
            return Frac(self.x + self.y*other, self.y)  # 1/5 + 2 = 11/5
        if other.y == self.y:
            return Frac(other.x+self.x, self.y)
        new_frac = add_frac([self.x, self.y], [other.x, other.y])
        return Frac(new_frac[0], new_frac[1])

    __radd__ = __add__              # int+frac

    def __sub__(self, other):  # frac1-frac2, frac-int
        if isinstance(other, float):
            ratio = float.as_integer_ratio(other)
            return self - Frac(ratio[0], ratio[1])
        if isinstance(other, int):
            return Frac(self.x - self.y*other, self.y)
        if other.y == self.y:
            return Frac(self.x - other.x, self.y)
        new_frac = sub_frac([self.x, self.y], [other.x, other.y])
        return Frac(new_frac[0], new_frac[1])

    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        return Frac(self.y * other - self.x, self.y)

    def __mul__(self, other):  # frac1*frac2, frac*int
        if isinstance(other, float):
            ratio = float.as_integer_ratio(other)
            return self * Frac(ratio[0], ratio[1])
        if isinstance(other, int):
            return Frac(self.x * other, self.y)
        return Frac(self.x * other.x, self.y * other.y)

    __rmul__ = __mul__              # int*frac

    def __div__(self, other):  # frac1/frac2, frac/int
        if isinstance(other, float):
            ratio = float.as_integer_ratio(other)
            return self / Frac(ratio[0], ratio[1])
        if isinstance(other, int):
            if other == 0:
                raise ZeroDivisionError
            other = Frac(other, 1)
        return self * ~other

    def __rdiv__(self, other):  # int/frac
        return other * ~self

    # operatory jednoargumentowe
    def __pos__(self):  # +frac
        return self

    def __neg__(self):         # -frac
        return Frac(-self.x, self.y)

    def __invert__(self):      # odwrotnosc: ~frac
        return Frac(self.y, self.x)

# Kod testujący moduł.

import unittest


class TestFrac(unittest.TestCase):
    def test_str(self):
        self.assertEqual(Frac(1, 3).__str__(), "1/3")
        self.assertEqual(Frac(3, 1).__str__(), "3")

    def test_repr(self):
        self.assertEqual(Frac(1, 3).__repr__(), "Frac(1, 3)")

    def test_denominator_zero(self):
        self.assertRaises(ValueError, callableObj=Frac, x=1, y=0)

    def test_wrong_value(self):
        self.assertRaises(ValueError, callableObj=Frac, x='asd', y=2)
        self.assertRaises(ValueError, callableObj=Frac, x=3, y='xxx')

    def test_add_fracs(self):
        result_same_denom = Frac(1, 2) + Frac(3, 2)
        result_common_division = Frac(1, 2) + Frac(3, 4)
        result_diff_denoms = Frac(1, 5) + Frac(2, 3)
        result_frac_int = Frac(1, 5) + 2

        self.assertEqual(result_same_denom, Frac(4, 2))
        self.assertEqual(result_common_division, Frac(5, 4))
        self.assertEqual(result_diff_denoms, Frac(13, 15))
        self.assertEqual(result_frac_int, Frac(11, 5))

    def test_radd(self):
        result_int_frac = 1 + Frac(1, 5)
        result_int_frac_zero = 0 + Frac(1, 5)
        self.assertEqual(result_int_frac, Frac(6, 5))
        self.assertEqual(result_int_frac_zero, Frac(1, 5))

    def test_sub_fracs(self):
        result_same_denom = Frac(1, 2) - Frac(3, 2)
        result_common_division = Frac(3, 4) - Frac(1, 2)
        result_diff_denoms = Frac(1, 5) - Frac(2, 3)
        result_frac_int = Frac(1, 5) - 2

        self.assertEqual(result_same_denom, Frac(-2, 2))
        self.assertEqual(result_common_division, Frac(1, 4))
        self.assertEqual(result_diff_denoms, Frac(-7, 15))
        self.assertEqual(result_frac_int, Frac(-9, 5))

    def test_rsub(self):
        result_int_frac = 1 - Frac(1, 5)
        result_int_frac_zero = 0 - Frac(1, 5)
        self.assertEqual(result_int_frac, Frac(4, 5))
        self.assertEqual(result_int_frac_zero, Frac(-1, 5))

    def test_mul_fracs(self):
        result1 = Frac(1, 2) * Frac(2, 3)
        result2 = Frac(-2, 3) * Frac(1, -3)
        result3 = Frac(3, 4) * 3
        result4 = 3 * Frac(3, 4)

        self.assertEqual(result1, Frac(2, 6))
        self.assertEqual(result2, Frac(-2, -9))
        self.assertEqual(result3, Frac(9, 4))
        self.assertEqual(result4, Frac(9, 4))

    def test_div(self):
        result1 = Frac(1, 2) / Frac(1, 2)
        result2 = Frac(3, 4) / Frac(2, 3)
        result3 = Frac(2, 4) / 2

        def zero_div():
            Frac(1, 2) / 0

        self.assertEqual(result1, Frac(2, 2))
        self.assertEqual(result2, Frac(9, 8))
        self.assertEqual(result3, Frac(2, 8))
        self.assertRaises(ZeroDivisionError, callableObj=zero_div)

    def test_rdiv(self):
        result = 2 / Frac(2, 4)
        self.assertEqual(result, Frac(8, 2))

    def test_neg(self):
        self.assertEqual(-Frac(-1, -2), Frac(-1, 2))
        self.assertEqual(-Frac(1, -2), Frac(1, 2))
        self.assertEqual(-Frac(-1, 2), Frac(1, 2))
        self.assertEqual(-Frac(1, 2), Frac(-1, 2))
        self.assertNotEqual(-Frac(-1, -2), Frac(-1, -2))

    def test_invert(self):
        self.assertEqual(~Frac(1, 2), Frac(2, 1))

    def test_add_float(self):
        result = Frac(1, 3) + 1.5
        self.assertEqual(result, Frac(11, 6))

    def test_subtract_float(self):
        result = Frac(1, 3) - 1.5
        self.assertEqual(result, Frac(-7, 6))

    def test_div_float(self):
        result = Frac(2, 3) / 1.5
        self.assertEqual(result, Frac(4, 9))

    def test_mul_float(self):
        result = Frac(1, 3) * 1.5
        self.assertEqual(result, Frac(3, 6))
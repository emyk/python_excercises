# -*- coding: utf-8 -*-
# zad 4.2

# b) 3.5
def make_spaces_for(number):
    return ' ' * (4 - len(str(number))+1)


def draw_measure():
    length = raw_input("Type measure length: ")
    try:
        length = int(length)
    except ValueError:
        return "You typed string. Please insert number"
    else:
        measure = "|...." * length + "|"
        numbers = "0"
        for i in xrange(1, length + 1):
            numbers += make_spaces_for(i)
            numbers += '{0}'.format(i)
        result = measure + "\n" + numbers
        return result

# print draw_measure()
# print '\n'


# a) 3.6
def draw_squares(height, width):
    horizontal = '+---' * width + '+'
    vertical = '|   ' * width + '|'
    up = horizontal + '\n' + vertical + '\n'
    rectangle = up * height + horizontal
    return rectangle

# print draw_squares(5, 5)
# print '\n'


# zad. 4.3
def factorial(n):
    result = 1
    for i in xrange(n+1):
        if i == 0:
            result *= 1
        else:
            result *= i
    return result

print factorial(0)


# zad. 4.4
def fibonacci(n):
    i_pp = 0
    i_p = 1
    if n == 0:
        return 0
    if n == 1:
        return 1

    for i in xrange(2, n+1):
        suma = i_pp + i_p
        i_pp = i_p
        i_p = suma
    return i_p

print fibonacci(11)


# zad. 4.5
def odwracanie_iter(L, left, right):
    o_ile = (right-left)/2
    iter_front = left
    iter_end = right
    for i in xrange(left, left+o_ile):
        if iter_front == iter_end:
            return L
        tmp = L[iter_front]
        L[iter_front] = L[iter_end]
        L[iter_end] = tmp
        iter_front += 1
        iter_end -= 1

    return L

L = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print odwracanie_iter(L, 3, 5), "iteracyjne \n"


def odwracanie_rekur(L, left, right):
    if left >= right:
        return L
    tmp = L[left]
    L[left] = L[right]
    L[right] = tmp
    return odwracanie_rekur(L, left+1, right-1)

L = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print odwracanie_rekur(L, 3, 5), "rekurencyjne \n"


# Zadanie 4.6
def from_subsequences__items_generator(sequence):
    for item in sequence:
        if isinstance(item, (list, tuple)):
            for x in from_subsequences__items_generator(item):
                yield x
        else:
            yield item


def sum_seq(sequence):
    return sum(from_subsequences__items_generator(sequence))


# Zadanie 4.7
def flatten(sequence):
    return list(from_subsequences__items_generator(sequence))

# coding=utf-8
# Emilia Marczyk, gr. 2
import math
import unittest

from zestaw_6.points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):         # "[(x1, y1), (x2, y2)]"
        return "[{pt1}, {pt2}]".format(pt1=self.pt1.__str__(), pt2=self.pt2.__str__())

    def __repr__(self):        # "Rectangle(x1, y1, x2, y2)"
        return "Rectangle{values}".format(values=(self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y))

    def __eq__(self, other):   # obsługa rect1 == rect2
        return id(other) == id(self)

    def __ne__(self, other):        # obsługa rect1 != rect2
        return not self == other

    def x_side_length(self):
        return math.fabs(self.pt1.x - self.pt2.x)

    def y_side_length(self):
        return math.fabs(self.pt1.y - self.pt2.y)

    def center(self):          # zwraca środek prostokąta
        x_half = float(self.x_side_length())/2
        y_half = float(self.y_side_length())/2

        x_min = min(self.pt1.x, self.pt2.x)
        y_min = min(self.pt1.y, self.pt2.y)
        return Point(x_min+x_half, y_min+y_half)

    def area(self):            # pole powierzchni
        return self.x_side_length() * self.y_side_length()

    def move(self, x, y):      # przesunięcie o (x, y)
        point = Point(x, y)
        self.pt1 += point
        self.pt2 += point

    def equals(self, other):
        return self.pt1.equals(other.pt1) and self.pt2.equals(other.pt2)


class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.p1 = Point(2, 3)
        self.p2 = Point(1, 1)
        self.p3 = Point(-4, 2)
        self.rect_above_zero = Rectangle(self.p1.x, self.p1.y, self.p2.x, self.p2.y)
        self.rect2 = Rectangle(self.p1.x, self.p1.y, self.p2.x, self.p2.y)
        self.rect_below_and_above = Rectangle(self.p3.x, self.p3.y, self.p2.x, self.p2.y)
        self.rect_below_zero = Rectangle(-1, -1, -4, -3)

    def test_str(self):
        expected = "[(2, 3), (1, 1)]"
        self.assertEqual(self.rect_above_zero.__str__(), expected)

    def test_repr(self):
        expected = "Rectangle(2, 3, 1, 1)"
        self.assertEqual(self.rect_above_zero.__repr__(), expected)

    def test_different_references_equality(self):
        self.assertNotEqual(self.rect_above_zero, self.rect2)

    def test_the_same_reference_equality(self):
        rect3 = self.rect_above_zero
        self.assertEqual(self.rect_above_zero, rect3)

    def test_x_side_length(self):
        self.assertEqual(self.rect_below_and_above.x_side_length(), 5)

    def test_y_side_length(self):
        self.assertEqual(self.rect_below_and_above.y_side_length(), 1)

    def test_center(self):
        self.assertEqual(self.rect_below_and_above.center().x, -1.5)
        self.assertEqual(self.rect_below_and_above.center().y, 1.5)
        self.assertEqual(self.rect_below_zero.center().x, -2.5)
        self.assertEqual(self.rect_below_zero.center().y, -2.0)

    def test_area(self):
        self.assertEqual(self.rect_above_zero.area(), 2)
        self.assertEqual(self.rect_below_zero.area(), 6)
        self.assertEqual(self.rect_below_and_above.area(), 5)

    def test_equals(self):
        self.assertTrue(self.rect_above_zero.equals(self.rect2))
        self.assertFalse(self.rect_below_and_above.equals(self.rect2))

    def test_move(self):
        self.rect_below_and_above.move(1, 1)
        self.rect_below_zero.move(-1, -1)
        self.assertTrue(self.rect_below_and_above.equals(Rectangle(-3, 3, 2, 2)))
        self.assertTrue(self.rect_below_zero.equals(Rectangle(-2, -2, -5, -4)))

from unittest import TestCase


# Zadanie 14.1
def list_nodes(graph):
    return graph.keys()


def list_edges(graph):
    L = []
    for source in graph:
        for target in graph[source]:
            L.append((source, target))
    return L


def count_nodes(graph):
    return len(list_nodes(graph))


def count_edges(graph):
    return len(list_edges(graph))


# Zadanie 14.6
def get_nodes_with_degrees(graph):
    nodes_with_weights = {node: 0 for node in graph.keys()}
    edges_used = []
    edges = list_edges(graph)
    for edge in edges:
        if not edge in edges_used:
            nodes_with_weights[edge[0]] += 1
            nodes_with_weights[edge[1]] += 1
            edges_used.append(edge)
            edges_used.append((edge[1], edge[0]))

    return nodes_with_weights


class GraphTest(TestCase):
    def setUp(self):
        self.graph = {
            "A": ["B", "C"],
            "B": ["C", "D"],
            "C": ["D"],
            "D": ["C"],
            "E": ["C"],
            "F": []
        }

    def test_list_nodes(self):
        expected_nodes = ["A", "B", "C", "D", "E", "F"]
        nodes = list_nodes(self.graph)
        self.assertListEqual(sorted(nodes), expected_nodes)

    def test_list_edges(self):
        edges = list_edges(self.graph)
        expected = [("A", "B"), ("A", "C"), ("B", "C"), ("B", "D"), ("C", "D"), ("D", "C"), ("E", "C")]
        self.assertEqual(len(edges), 7)
        for exp in expected:
            self.assertIn(exp, edges)

    def test_count_nodes(self):
        self.assertEqual(count_nodes(self.graph), 6)

    def test_count_edges(self):
        self.assertEqual(count_edges(self.graph), 7)

    def test_get_nodes_with_degrees(self):
        expected = {"A": 2, "B": 3, "C": 4, "D": 2, "E": 1, "F": 0}
        result = get_nodes_with_degrees(self.graph)
        self.assertEqual(result, expected)

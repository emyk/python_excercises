# coding=utf-8
# Emilia Marczyk, gr. 2
import math
import unittest


class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x=0, y=0):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self):         # zwraca string "(x, y)"
        return "({x}, {y})".format(x=self.x, y=self.y)

    def __repr__(self):        # zwraca string "Point(x, y)"
        return "Point({x}, {y})".format(x=self.x, y=self.y)

    def __eq__(self, other):   # obsługa point1 == point2
        return id(other) == id(self)

    def __ne__(self, other):        # obsługa point1 != point2
        return not self == other

    # Punkty jako wektory 2D.
    def __add__(self, other):  # v1 + v2
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):  # v1 - v2
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, other):  # v1 * v2, iloczyn skalarny
        return self.x * other.x + self.y * other.y

    def cross(self, other):         # v1 x v2, iloczyn wektorowy 2D
        return self.x * other.y - self.y * other.x

    def length(self):           # długość wektora
        return math.sqrt(self.x**2 + self.y**2)

    def equals(self, other):
        return self.x == other.x and self.y == other.y


class TestPoint(unittest.TestCase):
    def setUp(self):
        self.p1 = Point(1, 2)
        self.p2 = Point(1, 2)
        self.p3 = Point(3, 4)

    def test_points_with_different_references_equality(self):
        self.assertNotEqual(self.p1, self.p2)

    def test_points_with_same_reference_equality(self):
        p3 = self.p1
        self.assertEqual(self.p1, p3)

    def test_str(self):
        expected = "(1, 2)"
        self.assertEqual(self.p1.__str__(), expected)

    def test_repr(self):
        expected = "Point(1, 2)"
        self.assertEqual(self.p2.__repr__(), expected)

    def test_length(self):
        self.assertEqual(self.p3.length(), 5)

    def test_add(self):
        p1_p3 = self.p1 + self.p3
        point = Point(4, 6)

        self.assertEqual(p1_p3.x, point.x)
        self.assertEqual(p1_p3.y, point.y)

    def test_sub(self):
        p1_p3 = self.p3 - self.p1
        point = Point(2, 2)

        self.assertEqual(p1_p3.x, point.x)
        self.assertEqual(p1_p3.y, point.y)

    def test_mul(self):
        p1_p3 = self.p1 * self.p3
        result = 11
        self.assertEqual(p1_p3, result)

    def test_equal_values(self):
        self.assertTrue(self.p1.equals(self.p2))
        self.assertFalse(self.p1.equals(self.p3))

    def test_cross(self):
        self.assertEqual(self.p1.cross(self.p2), 0)
        self.assertEqual(self.p1.cross(self.p3), -2)

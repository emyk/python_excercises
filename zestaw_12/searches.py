from unittest import TestCase


# Zadanie 12.2
def binarne_rek(L, left, right, y):
    """Wyszukiwanie binarne w wersji rekurencyjnej."""
    k = (left + right) / 2
    if L[k] == y:
        return k
    if left == right:
        return -1
    if y < L[k]:
        return binarne_rek(L, left, k-1, y)
    else:
        return binarne_rek(L, k+1, right, y)


# Zadanie 12.6
def lider_py(L, left, right):
    occurences = {}
    for element in xrange(left, right):
        try:
            occurences[L[element]] += 1
        except KeyError:
            occurences.update({L[element]: 1})

    lider_candidate = max(occurences, key=occurences.get)
    if occurences[lider_candidate] > (right-left+1)/2:
        return lider_candidate

    return None


class SearchesTest(TestCase):
    def setUp(self):
        self.list_even = [2, 3, 5, 6, 8, 10]
        self.list_even_last = len(self.list_even) - 1
        self.list_odd = [3, 4, 6, 7, 8, 10, 13]
        self.list_odd_last = len(self.list_odd) - 1

    def test_binarne_rek(self):
        result1 = binarne_rek(self.list_even, 0, self.list_even_last, 3)
        result2 = binarne_rek(self.list_odd, 0, self.list_odd_last, 13)
        self.assertEqual(result1, 1)
        self.assertEqual(result2, self.list_odd_last)

    def test_binarne_rek_number_not_found(self):
        result = binarne_rek(self.list_even, 0, self.list_even_last, 4)
        self.assertEqual(result, -1)

    def test_lider(self):
        L = [1, 2, 1, 2, 1, 2, 3, 1, 1, 1, 1]
        result = lider_py(L, 0, len(L))
        self.assertEqual(result, 1)

    def test_no_lider(self):
        L = [1, 2, 1, 2, 1, 2, 3]
        result = lider_py(L, 0, len(L))
        self.assertIsNone(result)

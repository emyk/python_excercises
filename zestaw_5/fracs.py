# -*- coding: utf-8 -*-
import fractions


def _count_highest_common_multiple(a, b):
    a, b = (a, b) if b > a else (b, a)
    gcd = fractions.gcd(a, b)
    while b % a != 0:
        b = b * gcd
    return b


def get_new_denominator_and_numerators(frac1, frac2):
    numerator1, denom1 = frac1[0], frac1[1]
    numerator2, denom2 = frac2[0], frac2[1]

    if fractions.gcd(denom1, denom2) == 1:
        new_denom = denom1 * denom2
    else:
        new_denom = _count_highest_common_multiple(denom1, denom2)

    new_num1 = (new_denom/denom1) * numerator1
    new_num2 = (new_denom/denom2) * numerator2
    return new_denom, new_num1, new_num2


def add_frac(frac1, frac2):
    new_denom, new_num1, new_num2 = get_new_denominator_and_numerators(frac1, frac2)
    return [new_num1+new_num2, new_denom]


def sub_frac(frac1, frac2):
    new_denom, new_num1, new_num2 = get_new_denominator_and_numerators(frac1, frac2)
    return [new_num1-new_num2, new_denom]


def mul_frac(frac1, frac2):
    if is_zero(frac1) or is_zero(frac2):
        return [0, 1]
    return [frac1[0]*frac2[0], frac1[1]*frac2[1]]


def div_frac(frac1, frac2):
    if is_zero(frac2):
        raise ZeroDivisionError

    if is_zero(frac1):
        return [0, 1]
    return mul_frac(frac1, [frac2[1], frac2[0]])  # frac1 / frac2


def is_positive(frac):
    num = frac[0]
    denom = frac[1]

    if (num > 0 and denom > 0) or (num < 0 and denom < 0):
        return True
    return False


def is_zero(frac):  # bool, typu [0, x]
    if frac[0] == 0:
        return True
    return False


def cmp_frac(frac1, frac2):  # -1 | 0 | +1
    denominator, num1, num2 = get_new_denominator_and_numerators(frac1, frac2)

    if num1 == num2:
        return 0
    if num1 > num2:
        return 1
    if num1 < num2:
        return -1


def frac2float(frac):
    return round(float(frac[0]) / float(frac[1]), 2)   # konwersja do float

f1 = [-1, 2]                  # -1/2
f2 = [0, 1]                   # zero
f3 = [3, 1]                   # 3
f4 = [6, 2]                   # 3 (niejednoznaczność)
f5 = [0, 2]                   # zero (niejednoznaczność)


import unittest


class TestFractions(unittest.TestCase):
    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])
        self.assertEqual(add_frac([1, 6], [1, 3]), [3, 6])
        self.assertEqual(add_frac([-5, 6], [1, 3]), [-3, 6])
        self.assertEqual(add_frac(self.zero, [1, 3]), [1, 3])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([3, 4], [1, 6]), [7, 12])
        self.assertEqual(sub_frac([1, 6], [3, 4]), [-7, 12])
        self.assertEqual(sub_frac(self.zero, [3, 4]), [-3, 4])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([2, 5], [1, 3]), [2, 15])
        self.assertEqual(mul_frac(self.zero, [5, 6]), self.zero)

    def test_div_frac(self):
        self.assertEqual(div_frac([2, 5], [1, 3]), [6, 5])
        self.assertEqual(div_frac(self.zero, [1, 2]), self.zero)
        self.assertRaises(ZeroDivisionError, div_frac, [1, 2], self.zero)

    def test_is_positive(self):
        self.assertFalse(is_positive([-2, 4]))
        self.assertFalse(is_positive([2, -4]))
        self.assertTrue(is_positive([-1, -4]))
        self.assertTrue(is_positive([1, 4]))

    def test_is_zero(self):
        self.assertTrue(is_zero(self.zero))
        self.assertFalse(is_zero([5, 5]))

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([3, 5], [1, 2]), 1)
        self.assertEqual(cmp_frac([-1, 2], [3, 5]), -1)
        self.assertEqual(cmp_frac([9, 2], [18, 4]), 0)

    def test_frac2float(self):
        self.assertEqual(frac2float([1, 3]), 0.33)
        self.assertEqual(frac2float([1, -3]), -0.33)

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
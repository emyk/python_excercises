# -*- coding: utf-8 -*-
# zad 3.3
for i in xrange(31):
    if i % 3: print i

print '\n'

# zad 3.4
while True:
    user_input = raw_input("Type number ->  ")
    if user_input == "stop":
        break
    try:
        x = float(user_input.replace(',', '.'))
        print x, pow(x, 3)
    except ValueError:
        print "Error. You typed: '{0}'. Please, input number".format(user_input)

print '\n'

# zad 3.5
def make_spaces_for(number):
    return ' ' * (4 - len(str(number))+1)

def draw_measure():
    length = raw_input("Type measure length: ")
    try:
        length = int(length)
    except ValueError:
        return "You typed string. Please insert number"
    else:
        measure = "|...." * length + "|"
        numbers = "0"
        for i in xrange(1, length + 1):
            numbers += make_spaces_for(i)
            numbers += '{0}'.format(i)
        result = measure + "\n" + numbers
        return result

print draw_measure()
print '\n'

# zad 3.6
def draw_squares(height, width):
    horizontal = '+---' * width + '+'
    vertical = '|   ' * width + '|'
    up = horizontal + '\n' + vertical + '\n'
    rectangle = up * height + horizontal
    return rectangle

print draw_squares(5, 5)
print '\n'

# zad 3.8
a = ['a', 'b', 'a', 'b', 'c']
b = ['b', 'd', 'e', 'b', 'c']
print 'a: ', a
print 'b: ', b
print "Common elements: ", list(set(a).intersection(b))
print "Elements from both lists: ", list(set(a + b)), '\n'


# zad 3.9
example = [[], [4], (1, 2), [3, 4], (5, 6, 7)]
print "Example: ", example
print [sum(e) for e in example]

# zad 3.10
# Be aware that this program doesn't check if the roman number you type is correct
number = raw_input('Type roman number -> ')

def r2a(roman_number):
	roman_to_arabic = {
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000
	}
	
	return roman_to_arabic[roman_number]
	
def roman2int(roman_number):
    result = it = 0
    last_index = len(roman_number)
	
    while it < last_index:
        current = r2a(roman_number[it])
        try:
            next = r2a(roman_number[it+1])
        except Exception:
            if r2a(roman_number[it-1]) >= current:
                result += current
            else:
                result -= current
            return result
        if current >= next:
            result += current
            it += 1
        else:
            result += next
            result -= current
            it += 2

    return result

print roman2int(number)
	
